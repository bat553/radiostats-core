CREATE DATABASE  IF NOT EXISTS `radiostats` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `radiostats`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: radiostats
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entry`
--

DROP TABLE IF EXISTS `entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `morceau_id` int(11) NOT NULL,
  `artiste_id` int(11) NOT NULL,
  `radio` varchar(32) NOT NULL,
  `timestamp` datetime NOT NULL,
  `rolling_code` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `morceau_id` int(11) NOT NULL,
  `artiste_id` int(11) NOT NULL,
  `spotifyid` varchar(62) NOT NULL,
  `spotifyidart` varchar(62) NOT NULL,
  `deezerid` int(11) NOT NULL,
  `deezeridart` int(11) NOT NULL,
  `artiste` text NOT NULL,
  `morceau` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `link_ID_uindex` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `radios`
--

DROP TABLE IF EXISTS `radios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `radios` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `display_name` varchar(32) NOT NULL,
  `gender` text NOT NULL,
  `url` text NOT NULL,
  `playid` varchar(64) NOT NULL,
  `picurl` text NOT NULL,
  `size` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Dumping data for table `radios`
--

LOCK TABLES `radios` WRITE;
/*!40000 ALTER TABLE `radios` DISABLE KEYS */;
INSERT INTO `radios` VALUES (1,'funradio','Fun
Radio','Club, House, R\'n\'B','http://www.funradio.fr/','0DWhLqkhlWaAUB9n5oDjAF','https://upload.wikimedia.org/wikipedia/fr/e/eb/Fun_Radio.png','height=
\"92\" width=\"250\"'),(2,'mouv','Mouv\'','R\'n\'B, Hip Hop, Electro, Reggae,
Musique','http://www.mouv.fr/','7o1Oik5E1MjYUrkGHMNNSs','https://upload.wikimedia.org/wikipedia/fr/thumb/5/5d/Mouv.svg/1024px-Mouv.svg.png','height=\"92\"
width=\"250\"'),(3,'nrj','NRJ','Hits,
Mainstream','http://www.nrj.fr/','4QI88PYEPvNOCm9qNGArrd','https://upload.wikimedia.org/wikipedia/en/5/54/Logo_NRJ_2016.png',' height=\"192\" width=
\"200\"'),(4,'odsradio','ODS Radio','Oldies, Hits,
Musique','http://www.odsradio.com/','2pGvsMwczNZZqqnvUbM12g','https://upload.wikimedia.org/wikipedia/commons/4/47/ODS_Radio_Logo.png','height=\"140\" width=
\"300\" '),(5,'ouifm','OUI FM','Musique Alternative,
Rock','https://www.ouifm.fr/','0av28A2ORGECZn2eH8BQlo','https://upload.wikimedia.org/wikipedia/fr/thumb/3/33/Oui_FM_2014_logo.png/1024px-
Oui_FM_2014_logo.png','height=\"165\" width=\"180\" '),(6,'radiofg','Radio FG','Dance, Electro, House,
Musique','http://www.radiofg.com/','7oM4TLn4IqTXtreUlJqwye','https://upload.wikimedia.org/wikipedia/fr/7/7b/FG_DJ_Radio_logo_2001.png','height=\"120\"
width=\"270\"'),(7,'radioplus','La Radio Plus','House, Pop, R\'n\'B,
Musique','http://www.laradioplus.com/','2ixObPUt6sznbaZCH6iZxX','https://upload.wikimedia.org/wikipedia/commons/d/da/La_Radio_Plus_logo.png',' height=\"150\"
width=\"150\"'),(8,'skyrock','SkyRock','Rap, Hip Hop,
Musique','https://skyrock.fm/','599lWzEZs5MbCAeUCkcHQi','https://upload.wikimedia.org/wikipedia/fr/a/a5/Skyrock_logo_2011.png','height=\"95\" width=
\"200\"'),(9,'virginradio','Virgin Radio','Mainstream, Pop,
Rock','https://www.virginradio.fr/','7yeXK6rf7SezRwqIVTM6ad','https://upload.wikimedia.org/wikipedia/commons/d/d1/VirginRadio.png','height=\"150\" width=
\"200\"'),(10,'voltage','Voltage','Dance, Mainstream,
Musique','http://www.voltage.fr/','3Y1Mqsf6fJgLXHUIqEJGQM','https://upload.wikimedia.org/wikipedia/fr/b/bb/Voltage_logo_2011.png','height=\"170\" width=
\"200\"'),(12,'radiomeuh','Radiomeuh','Électique, Musique du Monde,
UnderGround','http://www.radiomeuh.com/','2lOsrGFM1J65SBAcgHUDv2','http://www.radiomeuh.com/wp-content/themes/meuh/images/profil_meuh.png','height=\"250\"
width=\"300\" '),(13,'couleur3','RTS - Couleur3','Musique Alternative Pop Rock
Suisse','http://programmesradio.rts.ch/couleur3/','4As81jr5SX1zfTD3flS5FR','https://upload.wikimedia.org/wikipedia/fr/thumb/6/6e/Couleur3.svg/1200px-
Couleur3.svg.png','width=\"350\" height=\"120\"'),(14,'z100','Z100 - New York\'s ','Mainstream, Top
40','https://z100.iheart.com/','57WW4rC9aRrrZQKMG3Hw2p','https://i.pinimg.com/originals/96/a1/d5/96a1d50c52f096378c4f2a9295f1ca16.jpg','width=\"350px\"
height=\"170px\"'),(15,'kiisfm','KIIS FM - Los Angeles','Top 40, Hits, Charts, News,
Carnival','http://www.kiisfm.com/main.html','2TFWUc3sv84oHGlZcdflhV','https://upload.wikimedia.org/wikipedia/commons/3/36/KIIS-FM_%28102.7_KIIS-FM
%29.jpg','');
/*!40000 ALTER TABLE `radios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET
TIME_ZONE=@OLD_TIME_ZONE */;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-17 19:10:52
