# Swano corp 2018 (c)
# Main file
"""RadioStats Backend Alpha 0.1 | Swano Corp 2018 (c)
 
 Usage:
     main.py <radio> [--flag]

 Arguments:
    radio            Nom de la radio à Crawler
    
 Options:
     -h --help       Affiche cette page
     --debug         Vérifie le fonctionnement du script (pas d'ajout a la bdd)
     --version       Affiche la version du module
 
 Radios Disponibles:
     testRadio
 
 (c) Swano 2018
"""
import os
import time
import sys
import re
from pytz import timezone
from docopt import docopt
import functions
from datetime import datetime

# Def heure de Paris
now_time = datetime.now(timezone('Europe/Paris'))
timestamp = now_time.strftime("%Y-%m-%d %H:%M:%S")

print('Initialisation...')
print(timestamp)
arguments = docopt(__doc__, version='Alpha 0.1')
time.sleep(1)

if __name__ == '__main__':
    print('Alpha 0.1 | Swano Corp 2018 (c)')
    if (sys.argv):
        RadioName = functions.Normaliser(sys.argv[1])
        RadioSpec = functions.RadioLoader(RadioName)
        RadioEnv = RadioSpec[0]
        DisplayName = RadioSpec[2]
        type = RadioSpec[3]
        url = RadioSpec[4]
        print('Gathering data from : ' + DisplayName + '...')
        functions.ElasticSyncPool(RadioName)
        print("Step 1 of 7 | Crawling")
        if (type == "json"):
            print('Fetching...')
            infos = functions.FetchJson(RadioName, RadioEnv, url)
            morceau = infos[0]
            artiste = infos[1]
            rolling_code = infos[2]
        elif(type == 'html'):
            print('Fetching...')
            infos = functions.FetchHtml(RadioName, RadioEnv, url)
            morceau = infos[0]
            artiste = infos[1]
            rolling_code = infos[2]
        elif(type == "xml"):
            print('Fetching...')
            infos = functions.FetchXml(RadioName, RadioEnv, url)
            morceau = infos[0]
            artiste = infos[1]
            rolling_code = infos[2]
        else:
            print('not found')

        print('Step 2 of 7 | Raw entry')
        functions.DatabaseInsertRaw(artiste, morceau, rolling_code, RadioName)

        print("Step 3 of 7 | Music Services Verification")
        infos = functions.DeezerQuery(artiste,morceau)
        if(infos=="Notfound"):
            print('Track not found in the Deezer database... Exiting...')
            print(morceau + ' ' + artiste)
            functions.ExitReport((morceau + ' ' + artiste + ' ' + RadioName), 'deezerDB')
            exit(2)
        DeezerID = infos[0]
        DeezerArtID = infos[1]
        infos = functions.SpotifyQuery(artiste, morceau)
        if(infos=="Notfound"):
            print('Track not found in the Spotify database... Exiting...')
            functions.ExitReport((morceau + ' ' + artiste + ' ' + RadioName), 'spotifyDB')
            print(morceau + ' ' + artiste)
            exit(2)
        artiste = infos[0]
        morceau = infos[1]
        SpotifyID = infos[2]
        SpotifyArtID = infos[3]


        print("Step 4 of 7 | Internal ID assignation")
        del(infos)
        infos = functions.SetInternalID(artiste, morceau, SpotifyID, SpotifyArtID, DeezerID, DeezerArtID)
        morceau_id = infos[0]
        artiste_id = infos[1]
        doublon = infos[2]

        print("Step 5 of 7 | Database Stockage")
        print(morceau_id, morceau, artiste, artiste_id, rolling_code, " spot ->", SpotifyArtID, SpotifyID, 'dee ->', DeezerArtID, DeezerID)
        _id = functions.DatabaseInsert(morceau, artiste, RadioName, rolling_code, SpotifyID)

        print("Step 6 of 7 | Elastic Sync")

        functions.ElasticSyncEntry(morceau_id, artiste_id, _id, RadioName)
        functions.ElasticSyncLink(morceau, artiste, morceau_id, artiste_id, SpotifyID, SpotifyArtID, DeezerID, DeezerArtID, doublon)

        print("Step 7 of 7 | Updating plylists")
        print('To Do'
              )
       # functions.SpotifyPlaylists()
    else:
        print('Wrong Entry')
        functions.ExitReport(sys.argv[0], 'bad_argvs')

print('END'
      ' ')
