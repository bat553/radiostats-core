# Swano Corp | 2018 (c)
# Function file
import requests
import time
import json
import lxml
import sys
import os
import re
import unidecode
import encodings
import xml.etree.ElementTree as ET
from requests.auth import HTTPBasicAuth
from time import gmtime, strftime
from pytz import timezone
from datetime import datetime
from bs4 import BeautifulSoup
import unidecode
from spotify import Client, OAuth
import spotify
from random import randint
import pymysql

# Def heure de Paris
now_time = datetime.now(timezone('Europe/Paris'))
timestamp = now_time.strftime("%Y-%m-%d %H:%M:%S")

dir_path = os.path.dirname(os.path.realpath(__file__))

# Mysql Connector
SQLCreds = json.load(open(dir_path + "/RadioEnv/mysql.json"))
conn = pymysql.connect(host=SQLCreds['host'], user=SQLCreds['user'], password=SQLCreds['password'],
                       db=SQLCreds['database'])
# End

# Spotify token
creds = json.load(open(dir_path + "/RadioEnv/SpotifyCred.json"))  # A MODIFIER
SpotifyAuth = OAuth(creds['client-id'], creds['client-secret'])
SpotifyAuth.request_client_credentials()


# End

def FetchJson(RadioName, RadioEnv, url):
    response = requests.get(url)
    HTTPStatus(response.status_code, response)
    data = response.json()

    morceau = eval(RadioEnv['radios'][RadioName]['morceau'])
    artiste = eval(RadioEnv['radios'][RadioName]['artiste'])
    rolling_code = eval(RadioEnv['radios'][RadioName]['rolling_code'])

    # Normalise les données (filtre)
    artiste = Normaliser(artiste)
    morceau = Normaliser(morceau)
    rolling_code = str(rolling_code)


    print(artiste, morceau, rolling_code)
    return (morceau, artiste, rolling_code)


def FetchHtml(RadioName, RadioEnv, url):
    response = requests.get(url)
    HTTPStatus(response.status_code, response)
    data = BeautifulSoup(response.text, "lxml")
    # data = data.prettify()

    artiste = data.find(lambda tag: eval(RadioEnv['radios'][RadioName]['artiste'])).text
    morceau = data.find(lambda tag: eval(RadioEnv['radios'][RadioName]['morceau'])).text
    rolling_code = data.find(lambda tag: eval(RadioEnv['radios'][RadioName]['rolling_code'])).text

    # Normalise les données (filtre)
    artiste = Normaliser(artiste)
    morceau = Normaliser(morceau)
    rolling_code = str(rolling_code)
    print(artiste, morceau, rolling_code)
    return (artiste, morceau, rolling_code)

def FetchXml(RadioName, RadioEnv, url):
    tree = ET.fromstring(requests.get(url).text)
    morceau = tree.find(eval(RadioEnv['radios'][RadioName]['morceau'])).text
    artiste = tree.find(eval(RadioEnv['radios'][RadioName]['artiste'])).text
    rolling_code = tree.find(eval(RadioEnv['radios'][RadioName]['rolling_code'])).text

    # Normalise les données (filtre)
    artiste = Normaliser(artiste)
    morceau = Normaliser(morceau)
    rolling_code = str(rolling_code)

    print(artiste, morceau, rolling_code)
    return(morceau,artiste,rolling_code)



def HTTPStatus(status_code, result):
    if status_code != 200:
        print('Status:', status_code, 'Problem with the request. Exiting.')
        print(result.text)
        exit()

def CheckData(data):
    if(data == 'doublon'):
        print('Data already crawled... Exiting...')
        exit(1)
    if(data):
        return
    else:
        print('Data not found... Exiting...')
        exit(1)


def RadioLoader(RadioName):
    RadioEnv = json.load(open(dir_path +"/RadioEnv/RadioEnv.json"))
    if (RadioName in RadioEnv['radios']):
        name = RadioEnv['radios'][RadioName]['name']
        DisplayName = RadioEnv['radios'][RadioName]['DisplayName']
        type = RadioEnv['radios'][RadioName]['type']
        url = RadioEnv['radios'][RadioName]['url']
        return (RadioEnv, name, DisplayName, type, url)
    else:
        print('Not Found!')
        exit()


def Normaliser(text):
    CheckData(text)
    text = unidecode.unidecode(text)
    text = re.sub(r'\([^)]*\)', '', text)
    text = text.lower()
    text = text.replace('.', '')
    text = text.replace("\r", '')
    #text = text.replace("'", ' ')
    text = text.replace('"', ' ')
    text = text.replace('ã®', 'i')
    text = text.replace('ã©', 'e')
    text = text.replace('[+]', ' ')
    text = text.replace('Ã«', 'e')
    text = text.replace('Ã¢', 'a')
    text = text.replace('Ã©', 'e')
    text = text.replace('ã¨', 'e')
    text = text.replace('ã', 'e')
    text = text.replace('&#039;', '')
    text = text.replace('vs', '')
    text = text.replace('(live)', '')
    text = text.replace('feat', ' ')
    text = text.replace('ft', ' ')
    text = text.replace('ft.', ' ')
    text = text.replace('&', ' ')
    text = text.replace('nrj hits', '')
    text = text.replace('hit music only', '')
    # text = text.replace(' ', ' ')

    # More coming

    return (text)


def SpotifyQuery(artiste, morceau):
    client = Client(SpotifyAuth)
    result = client.api.search(artiste + ' ' + morceau + ' NOT verison NOT instrumental NOT karaoke NOT cover NOT acoustic NOT tribute', 'track', 1)
    status_code = int(result['tracks']['total']) # Premiere verification (avec l'artiste)
    if (status_code):
        artiste = str(result['tracks']['items'][0]['artists'][0]['name'])
        morceau = str(result['tracks']['items'][0]['name'])
        SpotifyID = str(result['tracks']['items'][0]['id'])
        SpotifyArtID = str(result['tracks']['items'][0]['artists'][0]['id'])
        return (artiste, morceau, SpotifyID, SpotifyArtID)
    else:
        result = client.api.search(morceau + ' NOT verison NOT instrumental NOT karaoke NOT cover NOT acoustic NOT tribute', 'track', 1)
        status_code = result['tracks']['total']  # 2eme verification (sans l'artiste)
        if (status_code):
            artiste = str(result['tracks']['items'][0]['artists'][0]['name'])
            morceau = str(result['tracks']['items'][0]['name'])
            SpotifyID = str(result['tracks']['items'][0]['id'])
            SpotifyArtID = str(result['tracks']['items'][0]['artists'][0]['id'])
            return (artiste, morceau, SpotifyID, SpotifyArtID)
        else:
            print('Spotify Not Found !')
            exit()
            return ("Notfound")  # Toujous pas de résponse

def DeezerQuery(artiste, morceau):
    result = requests.get('https://api.deezer.com/search?q=artist:"' + artiste + '" track:"' + morceau + '"&order=RANKING&strict=on')
    HTTPStatus(result.status_code, result)
    result = result.json()
    if (result['total'] > 0):
        DeezerID = str(result['data'][0]['id'])
        DeezerArtID = str(result['data'][0]['artist']['id'])
        return (DeezerID, DeezerArtID)
    else:
        print('Deezer Notfound!')
        exit()
        return ("Notfound")  # Toujous pas de résponse


def SetInternalID(artiste, morceau, spotifyid, spotifyidart, deezerid, deezeridart):
    morceau_id = (10000000 + randint(1000000, 9999999))
    artiste_id = (20000000 + randint(1000000, 9999999))
    doublon = True
    try:
        with conn.cursor() as cursor:
            sql = "SELECT morceau_id, artiste_id FROM link WHERE `spotifyid`=%s AND `deezerid` = %s "
            cursor.execute(sql, (spotifyid, deezerid))
            if (not cursor.rowcount):
                # Si l'ID de l'artiste extiste déja, alors utilisé celui-ci
                sql = "SELECT `artiste_id`, `morceau_id` FROM `link` WHERE spotifyidart=%s AND `deezeridart`=%s"
                cursor.execute(sql, (spotifyidart, deezeridart))
                if (cursor.rowcount):
                    artiste_id = cursor.fetchone()[0]
                # Sinon garder l'ID généré


                sql = "INSERT INTO link (morceau_id, artiste_id, spotifyid, spotifyidart, deezerid, deezeridart, artiste, morceau) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (morceau_id, artiste_id, spotifyid, spotifyidart, deezerid, deezeridart, artiste, morceau))
                conn.commit()

            else:
                result = cursor.fetchone()
                print(result)
                morceau_id = result[0]
                artiste_id = result[1]
                print('doublon')
                doublon = False



    finally:
        print(' ')

    return (morceau_id, artiste_id, doublon)


def DatabaseInsert(morceau, artiste, radio, code, idspot):
    global timestamp
    code = str(code)
    current_code = 0
    _id = 0
    try:
        with conn.cursor() as cursor:
            sql = "SELECT * FROM link WHERE `spotifyid`=%s AND `morceau` = %s AND `artiste` = %s"
            cursor.execute(sql, (idspot, morceau, artiste))
            result = cursor.fetchone()
           # _id = result[0]
            morceau_id = result[1]
            artiste_id = result[2]
            sql = "SELECT `rolling_code` FROM entry WHERE `radio` = %s ORDER BY `timestamp` DESC LIMIT 1"
            cursor.execute(sql, radio)
            if(cursor.rowcount):
                current_code = cursor.fetchone()[0]
            if ((current_code != code) | (current_code == 0)):
                sql = "INSERT INTO entry (morceau_id, artiste_id, radio, timestamp, rolling_code) VALUES ( %s, %s, %s, %s, %s)"
                cursor.execute(sql, (morceau_id, artiste_id, radio, timestamp, code))
                conn.commit()
                cursor.execute("SELECT id FROM radiostats.entry WHERE morceau_id = %s ORDER BY id DESC LIMIT 1", morceau_id)
                result = cursor.fetchone()
                _id = result[0]
            else:
                print('doublon')
                return('doublon')
    finally:
        print(' ')
    if(_id):
        return (_id)

def DatabaseInsertRaw(artiste, morceau, code, radio):
    global timestamp
    current_code = code
    try:
        with conn.cursor() as cursor:
            sql = "SELECT `rolling_code` FROM raw WHERE `radio` = %s ORDER BY `timestamp` DESC LIMIT 1"
            cursor.execute(sql, radio)
            if(cursor.rowcount):
                current_code = cursor.fetchone()[0]
            else:
                current_code = timestamp
            if ((current_code != code) | (current_code == 0)):
                sql = "INSERT INTO raw (morceau, artiste, radio, timestamp, rolling_code) VALUES ( %s, %s, %s, %s, %s)"
                cursor.execute(sql, (morceau, artiste, radio, timestamp, code))
                conn.commit()
            else:
                print('doublon')
                return('doublon')
    finally:
        print(' ')



def ElasticSyncEntry(morceau_id, artiste_id, _id, radio):
    CheckData(_id)
    global timestamp
    data = {"date": timestamp, "morceau_id": morceau_id, "artiste_id": artiste_id, "radio": radio}
    result = requests.post(("http://10.8.0.2:9200/radio-entry/_doc/" + str(_id)), json=data, auth=HTTPBasicAuth('elastic', 'Swano74370!'))
    print('Query status : ' + str(result.status_code) + ", Id : " + str(_id))
    #print(result.text)

def ElasticSyncLink(morceau, artiste, morceau_id, artiste_id, spotifyid, spotifyidart, deezerid, deezeridart, doublon):
    CheckData(doublon)
    global timestamp
    data = {"first_entry":  timestamp, "morceau": morceau, "artiste": artiste, "morceau_id": morceau_id, "artiste_id": artiste_id,  "string": morceau + " " + artiste, "SpotifyId": spotifyid, "SpotifyArtId": spotifyidart, "DeezerId": deezerid, "DeezerArtId": deezeridart}
    result = requests.post(("http://10.8.0.2:9200/radio-link/_doc/" + str(morceau_id) ), json=data, auth=HTTPBasicAuth('elastic', 'Swano74370!'))
    print('Query status : ' + str(result.status_code) + ", Id : " + str(morceau_id))
    #print(result.text)

def ElasticSyncPool(radio):
    global timestamp
    data = {"pool_time":  timestamp, "pool_radio": radio}
    requests.post(("http://10.8.0.2:9200/radio-pool/_doc/" ), json=data, auth=HTTPBasicAuth('elastic', 'Swano74370!'))

def GetMorceauTop5000():
    data = {"size":0,"query":{"match_all":{}},"aggs":{"_doc":{"date_range":{"field":"date","format":"yyyy-MM-dd HH:mm:ss","ranges":[{"from":"now-7d/d","to":"now"}]},"aggs":{"_doc":{"terms":{"field":"morceau_id","size":5000}}}}}}
    result = requests.get(("http://10.8.0.2:9200/radio-entry/_doc/_search"), json=data, auth=HTTPBasicAuth('elastic', 'Swano74370!'))
    return result

def GetArtistTop5000():
    data = {"size":0,"query":{"match_all":{}},"aggs":{"_doc":{"date_range":{"field":"date","format":"yyyy-MM-dd HH:mm:ss","ranges":[{"from":"now-7d/d","to":"now"}]},"aggs":{"_doc":{"terms":{"field":"artiste_id","size":5000}}}}}}
    result = requests.get(("http://10.8.0.2:9200/radio-entry/_doc/_search"), json=data, auth=HTTPBasicAuth('elastic', 'Swano74370!'))
    return result

def ExitReport(raw, breakpoint):
    global timestamp
    data = {"timestamp":  timestamp, "raw_infos": raw, "breakpoint": breakpoint}
    result = requests.post(("http://10.8.0.2:9200/radio-errors/_doc"), json=data, auth=HTTPBasicAuth('elastic', 'Swano74370!'))
    return result
