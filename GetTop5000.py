# Swano Corp (c) 2018 | GetTop5000.py
# Récuperation du Top5000 1 fois par jours
import requests
import functions
import json


print('Recovering Top 5000')
result = functions.GetMorceauTop5000()
json_string = result.text
json_string = json.loads(json_string)
print(' ')
print('Took : ', json_string["took"])
json_string = json_string["aggregations"]["_doc"]["buckets"][0]["_doc"]["buckets"]
print('Writing result through : /var/www/rest/top5000.json')
file = open("/var/www/rest/top5000.json", 'w')
file.write(str(json_string))
file.close()
print(' ')
print('All done !')

print('Recovering Top Artists 5000')
result = functions.GetArtistTop5000()
json_string = result.text
json_string = json.loads(json_string)
print(' ')
print('Took : ', json_string["took"])
json_string = json_string["aggregations"]["_doc"]["buckets"][0]["_doc"]["buckets"]
print('Writing result through : /var/www/rest/top5000-art.json')
file = open("/var/www/rest/top5000.json-art", 'w')
file.write(str(json_string))
file.close()
print(' ')
print('All done !')


