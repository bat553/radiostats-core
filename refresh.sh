#!/usr/bin/env bash
echo 'Start'
echo ' '
python3 /var/python/venv/main.py ouifm
echo ' '
python3 /var/python/venv/main.py funradio
echo ' '
python3 /var/python/venv/main.py skyrock
echo ' '
python3 /var/python/venv/main.py radioplus
echo ' '
python3 /var/python/venv/main.py radiomeuh
echo ' '
python3 /var/python/venv/main.py z100
echo ' '
python3 /var/python/venv/main.py nrj
echo ' '
python3 /var/python/venv/main.py mouv
echo ' '
#python3 main.py voltage # rolling code problem
echo ' '
python3 /var/python/venv/main.py odsradio
echo ' '
python3 /var/python/venv/main.py radiolac
echo ' '
python3 /var/python/venv/main.py radiofg
echo ' '
echo 'END'
